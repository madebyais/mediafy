const Logo = () => (
  <div className={`text-primary font-bold uppercase text-center tracking-wider text-lg`}>Mediafy</div>
)

export default Logo