import {NextApiRequest, NextApiResponse} from "next";
import S3Service from "services/s3";
import {BucketStream} from "minio";

export default async function api(req: NextApiRequest, res: NextApiResponse) {
  const s3Service: S3Service = new S3Service(process.env)
  const prefix: string = `${s3Service.getPrefix()}/`
  const stream: BucketStream<any> = s3Service.client().listObjectsV2(s3Service.getBucket(), prefix, false)

  await new Promise((resolve) => {
    let error: any = undefined
    let folders: Array<any> = []

    stream.on('data', (data: any) => {
      if ((data.prefix && data.prefix.indexOf(`undefined`) < 0) && data.size == 0) {
        folders.push({
          ...data,
          folderName: data.prefix.replace(prefix, '').replace(`/`, ''),
          folderPath: data.prefix
        })
      }
    })
    stream.on('error', (e: any) => error = e)
    stream.on('end', () => {
      if (error) res.status(500).json({ success: false, error: error })
      else res.status(200).json({ success: true, data: folders})
      resolve(() => {})
    })
  })
}