import {NextApiRequest, NextApiResponse} from "next";
import S3Service from "services/s3";
import NextConnect from "next-connect";
import multiparty from "multiparty";
import {v1 as uuidv1} from "uuid";
import {readFileSync} from "fs";

const middleware: any = NextConnect().use(async (req: NextApiRequest, res: NextApiResponse, next: any) => {
  const form = new multiparty.Form()

  await form.parse(req, function (err, fields, files) {
    req.body = {...fields, ...files}
    next()
  })
})

export default NextConnect()
  .use(middleware)
  .post(async (req: NextApiRequest, res: NextApiResponse) => {
    let {file: files, folderId}: any = req.body

    if (!folderId) return res.status(401).json({ success: false, error: 'Unauthorized access.' })
    if (files.length == 0) return res.status(400).json({ success: false, error: 'Unable to find uploaded file.' })
    folderId = folderId[0]

    const file = files[0]
    const {path, headers, size, originalFilename} = file

    const maxFileSize: number = parseInt(process.env.S3_MAX_FILE_SIZE_BYTES!.toString())
    if (size > maxFileSize) {
      return res.status(400).json({ success: false, error: `Max file size is only ${maxFileSize/1000000}MB` })
    }

    const fileExtension: string = originalFilename.split('.')[originalFilename.split('.').length-1]
    const whitelistedFileTypes: Array<string> = process.env.S3_WHITELIST_FILE_TYPES!.toString().split(',')
    if (whitelistedFileTypes.indexOf(fileExtension) < 0) {
      return res.status(400).json({ success: false, error: `File type "${fileExtension}" is not allowed to be uploaded.` })
    }

    const s3Service: S3Service = new S3Service(process.env)
    const prefix: string = `${s3Service.getPrefix()}/${folderId}/${originalFilename.replace(/\s/gi, '-')}`
    const metadata: any = {
      ...headers,
      'Content-Length': size,
      'x-amz-acl': 'public-read'
    }

    try {
      const result: any = await s3Service.client().fPutObject(s3Service.getBucket(), prefix, path, metadata)
      res.status(200).json({ success: true, data: result })
    } catch (e) {
      res.status(500).json({ success: false, error: e })
    }
  })

export const config = {
  api: {
    bodyParser: false
  }
}