import {NextApiRequest, NextApiResponse} from "next";
import S3Service from "services/s3";

export default async function api(req: NextApiRequest, res: NextApiResponse) {
  if (!req.query.folderId) {
    return res.status(401).json({ success: false, error: 'Unauthorized access.' })
  }

  const s3Service: S3Service = new S3Service(process.env)
  const prefix: string = `${s3Service.getPrefix()}/${req.query.folderId}/.init`
  try {
    const result: any = await s3Service.client().putObject(s3Service.getBucket(), prefix, '')
    res.status(200).json({ success: true, data: result })
  } catch (e) {
    console.dir(e)
    res.status(500).json({ success: false, error: e })
  }
}