import Fetch from "util/fetch";
import {Button, Table} from "antd";
import moment from "moment";
import Logo from "../components/icon/logo";

export default function Home({ success, data }: any) {
  return (
    <div className={`container mx-auto py-5`}>
      <a href={`/`}><Logo /></a>
      <div className={`my-5`}>
        <Table columns={[
          { key: 'folderName', dataIndex: 'folderName', title: 'Folder' },
          { key: 'lastModified', dataIndex: 'lastModified', title: 'Last Modified', render: (lastModified) => moment(lastModified).format(`DD-MM-YYYY HH:mm:ss`)  },
          { key: 'folderName', dataIndex: 'folderName', title: '', render: (folderName) => <Button type={`primary`} onClick={e => window.location.href = `/${folderName}`}>View</Button> }
        ]} dataSource={data} rowKey={`folderName`} />
      </div>
    </div>
  )
}

export async function getServerSideProps() {
  const result: any = await Fetch('GET', `${process.env.BASE_URL}/api/v1/s3/list-prefix`)
  return {
    props: { ...result }
  }
}